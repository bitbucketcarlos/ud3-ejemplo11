package com.example.ud3_ejemplo11;


import android.os.Parcel;
import android.os.Parcelable;

/* Implementamos la interfaz Parcelable para poder pasar el objeto creado de una actividad a otra.
   Para ello es necesario implementar el atributo CREATOR e implmenetar los métodos describeContents
   y writeToParcel. */
public class Persona implements Parcelable {
    private String nombre;
    private String apellido;

    public Persona(String nombre, String apellido) {
        this.nombre = nombre;
        this.apellido = apellido;
    }

    protected Persona(Parcel in) {
        nombre = in.readString();
        apellido = in.readString();
    }

    public static final Creator<Persona> CREATOR = new Creator<Persona>() {
        @Override
        public Persona createFromParcel(Parcel in) {
            return new Persona(in);
        }

        @Override
        public Persona[] newArray(int size) {
            return new Persona[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.nombre);
        parcel.writeString(this.apellido);
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
}

